class RemoveMonsterIdFromTeams < ActiveRecord::Migration
  def change
    remove_column :teams, :monster_id
  end
end
