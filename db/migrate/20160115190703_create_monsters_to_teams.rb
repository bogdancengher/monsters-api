class CreateMonstersToTeams < ActiveRecord::Migration
  def change
    create_table :monsters_to_teams do |t|
      t.integer :monster_id
      t.integer :team_id

      t.timestamps null: false
    end
  end
end
