class Api::MonstersController < Api::BaseController

  def index
    dir = params[:dir] || "asc"
    puts dir
    monsters = case params[:order]
    when nil
       @current_user.monsters
    when "weakness"
      @current_user.monsters.sort do |a, b|
        winner = winner(a.monster_type, b.monster_type)
        case
        when a.monster_type.to_sym == winner
          dir == "asc" ? 1 : -1
        when b.monster_type.to_sym == winner
          dir == "asc" ? -1 : 1
        end
      end
    else
      @current_user.monsters.order("#{params[:order]} #{dir}")
    end
      
    render json: {monsters: monsters}
  end

  def create
    monster = @current_user.monsters.new(monster_params)
    if monster.save
      render json: create_response(true, monster) and return
    end
    render json: create_response(false, nil, monster.errors.full_messages), status: :unprocessable_entity
  end

  def edit
    monster = Monster.find(params[:id])
    render json: create_response(true, monster)
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def update
    monster = Monster.find(params[:id])
    if monster.update_attributes(monster_params)
      render json: create_response(true, monster) and return
    end
    render json: create_response(false, nil, monster.errors.full_messages), status: :unprocessable_entity
  rescue ActiveRecord::RecordNotFound  
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def destroy
    monster = Monster.find(params[:id])
    monster.destroy
    render json: create_response(true, nil, ["Monster successfully destroyed"])
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def winner(p1, p2)
    Monster::RULES[p1.to_sym][p2.to_sym]
  end

  private

  def monster_params
    params.require(:monster).permit(:name, :power, :monster_type)
  end
end