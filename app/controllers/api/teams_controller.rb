class Api::TeamsController < Api::BaseController
  def index
    teams = @current_user.teams
    render json: {teams: teams}
  end

  def create
    team = @current_user.teams.new(team_params)
    if team.save
      render json: create_response(true, team) and return
    end
    render json: create_response(false, nil, team.errors.full_messages), status: :unprocessable_entity
  end

  def edit
    team = Team.find(params[:id])
    render json: create_response(true, team)
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def update
    team = Team.find(params[:id])
    if team.update_attributes(team_params)
      render json: create_response(true, team) and return
    end
    render json: create_response(false, nil, team.errors.full_messages), status: :unprocessable_entity
  rescue ActiveRecord::RecordNotFound  
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def destroy
    team = Team.find(params[:id])
    team.destroy
    render json: create_response(true, nil, ["Team successfully destroyed"])
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def monsters
    team = Team.find(params[:id])
    render json: {monsters: team.monsters}
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  def add_monster
    team = Team.find(params[:id])
    if team.monsters.count == 3
      render json: create_response(false, team, ["You can only have 3 monsters in a team"]), status: :unprocessable_entity and return
    end
    monster = Monster.find(team_params[:monster_id])
    team.monsters << monster
    render json: create_response(true, nil, ["Monster successfully added to team"])
  rescue ActiveRecord::RecordNotFound
    render json: create_response(false, nil, ["Record not found."]), status: :not_found
  end

  private

  def team_params
    params.require(:team).permit(:name, :monster_id)
  end
end