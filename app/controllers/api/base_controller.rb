class Api::BaseController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  before_filter :authenticate_user
  respond_to :json

  protected

  def authenticate_user
    if params[:uid].present?
      @current_user = User.find_by_uid(params[:uid])

      if @current_user.blank?
        render json: {error: "User not found."}, status: :unauthorized
      end
    else
      render json: {error: "Missing facebook id."}, status: :not_acceptable
    end
  end

  def create_response(success, object, errors = [])
    {success: success, object: object, errors: errors}
  end 

end