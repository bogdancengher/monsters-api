class User < ActiveRecord::Base
  has_many :monsters, dependent: :destroy
  has_many :teams, dependent: :destroy

  def self.find_or_create_by_auth(auth_data)
    user = self.find_or_create_by(provider: auth_data["provider"], uid: auth_data["uid"])
    if user.email != auth_data.extra.raw_info.email
      user.email = auth_data.extra.raw_info.email
      user.name = auth_data.extra.raw_info.name
      user.save
    end
    return user
  end
end
